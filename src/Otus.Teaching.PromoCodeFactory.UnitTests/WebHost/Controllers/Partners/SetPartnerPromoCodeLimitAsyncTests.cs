﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.MethodsFactory;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Linq;
using System;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = new PartnersController(_partnersRepositoryMock.Object);
        }
        //Если партнер не найден, то также нужно выдать ошибку 404;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PatnerIsNotFound_ReturnNotFound()
        {
            //Private Arrange
            IMethodsFactory factory = new PartnerNotFound();

            //Arrange
            _partnersRepositoryMock.Setup(partnerRepo => partnerRepo.GetByIdAsync(factory.GetGuid())).ReturnsAsync(factory.GetPartner());

            // Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(factory.GetGuid(), factory.GetRequest());

            // Assert
            factory.Check(result);
        }
        //Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNonActive_ReturnNonActive()
        {
            //Private Arrange
            IMethodsFactory factory = new NotActivePartner();

            //Arrange
            _partnersRepositoryMock.Setup(partnerRepo => partnerRepo.GetByIdAsync(factory.GetPartner().Id)).ReturnsAsync(factory.GetPartner());

            //Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(factory.GetPartner().Id, factory.GetRequest());

            // Assert
            factory.Check(result);
        }

        //Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerSetLimit_NumberPromoCodesSetZero()
        {
            IMethodsFactory factory = new PartnerSetLimit();
            Partner partner = factory.GetPartner();

            _partnersRepositoryMock.Setup(partnerRepo => partnerRepo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, factory.GetRequest());

            // Assert
            factory.Check(result);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        // Если лим закончился, то кол-во не обнуляем
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_IfLimitFinished_NumberPromoCodesSetNotZero()
        {
            IMethodsFactory factory = new PartnerSetLimit();
            Partner partner = factory.GetPartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue).CancelDate = DateTime.Now.AddDays(-1);
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, factory.GetRequest());

            // Assert
            factory.Check(result);

            partner.NumberIssuedPromoCodes.Should().NotBe(0);
        }



        //При установке лимита нужно отключить предыдущий лимит;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerResetLimit_DisablePreviousLimit()
        {
            IMethodsFactory factory = new PartnerSetLimit();
            Partner partner = factory.GetPartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            PartnerPromoCodeLimit lastActiveLimit = partner.PartnerLimits.LastOrDefault(x => !x.CancelDate.HasValue);
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, factory.GetRequest());

            // Assert
            factory.Check(result);

            lastActiveLimit.CancelDate.HasValue.Should().BeTrue();
        }

        //Лимит должен быть больше 0;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_SetLimitGreaterZero_ReturnBadResult()
        {
            IMethodsFactory factory = new LimitGreaterZero();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(factory.GetPartner().Id)).ReturnsAsync(factory.GetPartner());

            // Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(factory.GetPartner().Id, factory.GetRequest());

            // Assert
            factory.Check(result);

        }

        //Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_SaveNewLimitInDb_LimitExists()
        {
            IMethodsFactory factory = new LimitGreaterZero();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(factory.GetPartner().Id)).ReturnsAsync(factory.GetPartner());

            //Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(factory.GetPartner().Id, factory.GetRequest());

            //Assert
            factory.Check(result);

            Partner partnerInDb = await _partnersRepositoryMock.Object.GetByIdAsync(factory.GetPartner().Id);
            partnerInDb.PartnerLimits.Should().NotBeEmpty();
        }

    }
}