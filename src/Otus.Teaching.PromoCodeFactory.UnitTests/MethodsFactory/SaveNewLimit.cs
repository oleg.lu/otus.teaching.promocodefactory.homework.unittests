﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.MethodsFactory
{
    public class SaveNewLimit : IMethodsFactory
    {
        public void Check(IActionResult res)
        {
            res.Should().BeAssignableTo<CreatedAtActionResult>();
        }

        public Guid GetGuid()
        {
            return It.IsAny<Guid>();
        }
        public Partner GetPartner()
        {
            return new Partner()
            {
                Id = Guid.Parse("35e4406c-31a4-4dd5-b67d-f13c0dd1d3dc"),
                Name = "Jewerly",
                IsActive = true,
                NumberIssuedPromoCodes = 1,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("4bb3cf36-082f-45d4-8a90-b8c8d26c31d3"),
                        CreateDate = new DateTime(2024, 01, 01),
                        EndDate = new DateTime(2025, 01, 01),
                        Limit = 100
                    }
                }
            };
        }

        public SetPartnerPromoCodeLimitRequest GetRequest()
        {
            return new SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Now.AddDays(30), Limit = 1 };
        }
    }
}
