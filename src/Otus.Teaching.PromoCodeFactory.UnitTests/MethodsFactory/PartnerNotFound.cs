﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.MethodsFactory
{
    public class PartnerNotFound : IMethodsFactory
    {
        public void Check(IActionResult res)
        {
            res.Should().BeAssignableTo<NotFoundResult>();
        }

        public Guid GetGuid()
        {
            return It.IsAny<Guid>();
        }

        public Partner GetPartner()
        {
            return null;
        }

        public SetPartnerPromoCodeLimitRequest GetRequest()
        {
            return It.IsAny<SetPartnerPromoCodeLimitRequest>();
        }
    }
}
