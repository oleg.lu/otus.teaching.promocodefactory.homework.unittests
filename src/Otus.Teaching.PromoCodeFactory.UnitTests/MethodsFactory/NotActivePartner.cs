﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.MethodsFactory
{
    public class NotActivePartner : IMethodsFactory
    {
        public void Check(IActionResult res)
        {
            res.Should().BeAssignableTo<BadRequestObjectResult>();
            (res as BadRequestObjectResult).Value.Should().Be("Данный партнер не активен");
        }

        public Guid GetGuid()
        {
            return It.IsAny<Guid>();
        }

        public Partner GetPartner()
        {
            return new Partner() { Id = Guid.NewGuid(), IsActive = false };
        }

        public SetPartnerPromoCodeLimitRequest GetRequest()
        {
            return It.IsAny<SetPartnerPromoCodeLimitRequest>();
        }
    }
}
