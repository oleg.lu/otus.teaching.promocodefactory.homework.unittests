﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.MethodsFactory
{
    internal interface IMethodsFactory
    {
        /// <summary>
        /// Получает GUID партнера
        /// </summary>
        /// <returns>Возвразает объект типа Guid</returns>
        public Guid GetGuid();

        /// <summary>
        /// Получает объект партнера
        /// </summary>
        /// <returns>Возвращает объект Partner</returns>
        public Partner GetPartner();

        /// <summary>
        /// Получает объект запроса на установку лимита
        /// </summary>
        /// <returns>Возвращает объект типа SetPartnerPromoCodeLimitRequest</returns>
        public SetPartnerPromoCodeLimitRequest GetRequest();

        /// <summary>
        /// Проверка результата запроса
        /// </summary>
        /// <param name="res">Результат в методе установки лимита</param>
        public void Check(IActionResult res);
    }
}
